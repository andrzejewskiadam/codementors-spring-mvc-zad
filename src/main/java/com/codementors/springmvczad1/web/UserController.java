package com.codementors.springmvczad1.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codementors.springmvczad1.model.User;
import com.codementors.springmvczad1.model.UserId;
import com.codementors.springmvczad1.model.UserIdWithUsername;
import com.codementors.springmvczad1.model.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping("/list")
  public List<User> listAllUsers() {
    return userRepository.findAll();
  }

  @PostMapping({ "", "/add" })
  public ResponseEntity<UserId> addUser(@RequestBody final User user) {
    final User byUsername = userRepository.findByUsername(user.getUsername());
    if (byUsername == null) {
      final User saved = userRepository.save(user);
      return ResponseEntity.ok(new UserId(saved.getId()));
    }
    return ResponseEntity.status(HttpStatus.CONFLICT).build();
  }

  @PostMapping("/addAll")
  public ResponseEntity<List<UserIdWithUsername>> addUsers(@RequestBody final List<User> users) {
    if (users == null || users.isEmpty()) {
      return ResponseEntity.badRequest().build();
    }
    final List<UserIdWithUsername> savedUsers = new ArrayList<>();
    users.forEach(user -> {
      final User byUsername = userRepository.findByUsername(user.getUsername());
      if (byUsername == null) {
        final User saved = userRepository.save(user);
        savedUsers.add(new UserIdWithUsername(saved.getId(), saved.getUsername()));
      }
    });
    return ResponseEntity.ok(savedUsers);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateUser(@PathVariable("id") final Long userId, @RequestBody final User user) {
    final User original = userRepository.findOne(userId);
    if (original == null) {
      return ResponseEntity.notFound().build();
    }
    update(original, user);
    return ResponseEntity.ok().build();
  }

  @GetMapping
  public List<User> findByExample(@RequestParam(name = "username", required = false) final String username,
                                  @RequestParam(name = "firstname", required = false) final String firstname,
                                  @RequestParam(name = "age", required = false) final Integer age) {
    final User example = new User();
    example.setFirstname(firstname);
    example.setAge(age);
    example.setUsername(username);
    final List<User> found = userRepository.findAll(Example.of(example, ExampleMatcher.matchingAny()));
    return found;
  }

  private void update(final User original, final User user) {
    //copy field values
    if (user.getFirstname() != null) {
      original.setFirstname(user.getFirstname());
    }
    userRepository.save(original);
  }
}
