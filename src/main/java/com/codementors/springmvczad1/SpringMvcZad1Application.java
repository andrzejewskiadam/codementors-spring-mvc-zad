package com.codementors.springmvczad1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcZad1Application {


  public static void main(final String[] args) {
    SpringApplication.run(SpringMvcZad1Application.class, args);
  }

}

