package com.codementors.springmvczad1;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.codementors.springmvczad1.model.UserRepository;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
        .antMatchers("/css/**", "/index").permitAll()
        .antMatchers("/user/**").hasRole("USER")
        .and().csrf().disable()
        .httpBasic();
  }

  //  @Autowired
//  public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
//    auth
//        .inMemoryAuthentication()
//        .withUser("user").password("password").roles("USER");
//  }
  @Autowired
  public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
    final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setUserDetailsService(userDetailsServiceBean());
    auth.authenticationProvider(authenticationProvider);
  }

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetailsService userDetailsServiceBean() throws Exception {
    return new UserDetailsService() {
      @Override
      public UserDetails loadUserByUsername(final String username)
          throws UsernameNotFoundException {
        //load from db user based on name
        //return userService.getUserDetails(userName);
        if (userRepository.findByUsername(username) == null) {
          throw new UsernameNotFoundException("User " + username + " not found");
        }
        return new User(username, "password",
            Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
      }
    };
  }


}
