package com.codementors.springmvczad1.model;

public class UserIdWithUsername {
  private Long id;
  private String username;

  public UserIdWithUsername() {
  }

  public UserIdWithUsername(final Long id, final String username) {
    this.id = id;
    this.username = username;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

}
