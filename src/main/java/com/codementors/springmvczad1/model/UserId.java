package com.codementors.springmvczad1.model;

public class UserId {
  private Long id;

  public UserId() {
  }

  public UserId(final Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }
}
